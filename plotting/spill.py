#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Methos for making spill realted plots

"""

__author__ = "Philipp Niedermayer"
__contact__ = "p.niedermayer@gsi.de"
__date__ = "2022-08-19"



from .common import *
from .other import format_axis_radians




def plot_spill_intensity(ax, spill_data, bin_time=10e-6, *, rate=False, cumulative=False, relative=False, swap_axis=False, **plot_kwargs):
    """Plot spill intensity (binned particle counts) over extraction time
    
    :param ax: Axis to plot onto
    :param spill_data: Instance of spill data class, e.g. TDCSpill
    :param bin_time: Width of bins for particle counting in sec
    :param rate: If true, plot particle count rate, i.e. counts per s instead of per bin
    :param cumulative: If true, plot cumulative particle number
    :param relative: If true, plot fraction of total particle number
    :param swap_axis: If True, swap plot axis
    :param plot_kwargs: Keyword arguments to be passed to plotting method
    """
    assert not (rate and cumulative), 'rate and cumulative plotting are mutually exclusive'
    
    # histogram in time bins
    nbins = int(np.ceil(spill_data.length/bin_time))
    weights = np.ones_like(spill_data.hittimes)
    if rate: weights /= bin_time
    if relative: weights /= spill_data.counts
    hist, edges = np.histogram(spill_data.hittimes, bins=nbins, weights=weights, 
                               range=(spill_data.time_offset, spill_data.time_offset+bin_time*nbins))
    if cumulative: hist = np.cumsum(hist)
    
    # plot
    ax.stairs(hist, edges, baseline=None if cumulative else 0, orientation='horizontal' if swap_axis else 'vertical', **plot_kwargs)
    if relative: (ax.xaxis if swap_axis else ax.yaxis).set_major_formatter(mpl.ticker.PercentFormatter(1))
    ax_set(ax, swap_axis,
           xlabel='Extraction time / s',
           ylabel='Extracted particle' + \
                   (' fraction' if relative else 's') + \
                   ('' if cumulative else \
                        (' / s' if rate else f'\nper ${(bin_time*SI.s).to_compact():~gL}$ interval')),
          )




def plot_spill_duty_factor(ax, spill_data, counting_dt=10e-6, evaluation_dt=10e-3, *, show_poisson_limit=False, **plot_kwargs):
    """Plot spill duty factor (<x>²/<x²>) over extraction time
    
    :param ax: Axis to plot onto
    :param spill_data: Instance of spill data class, e.g. TDCSpill
    :param counting_dt: Width of bins for particle counting in sec
    :param evaluation_dt: Width of bins for duty factor evaluation (value is rounded to the next multiple of count_bin_time) 
    :param show_poisson_limit: If true, show poison limit of duty factor
    :param plot_kwargs: Keyword arguments to be passed to plotting method
    """
    
    counts, edges = spill_data.hitcounts(counting_dt, evaluation_dt)
    
    # spill duty factor
    F = np.mean(counts, axis=1)**2 / np.mean(counts**2, axis=1)
    s = ax.stairs(F, edges, **plot_kwargs)
    
    if show_poisson_limit:
        Fp = np.mean(counts, axis=1) / ( np.mean(counts, axis=1) + 1 )
        ax.stairs(Fp, edges, label='Poisson limit', color=s.get_edgecolor() or 'gray', alpha=0.5,zorder=-1, lw=1, ls=':')
        
    ax.set(xlabel='Extraction time / s', ylim=(0,1.1),
           ylabel=f'Spill duty factor F\n(${(evaluation_dt*SI.s).to_compact():~gL}$ / ${(counting_dt*SI.s).to_compact():~gL}$ intervals)'
    )




def plot_spill_consecutive_particle_delay(ax, spill_data, limit=5e-6, resolution=10e-9, show_poisson=False, swap_axis=False, log=True,
                                          **plot_kwargs):
    """Plot histogram of consecutive particle separation (particle-interval histograms)
    
    :param ax: Axis to plot onto
    :param spill_data: Instance of TDCSpill
    :param limit: Maximum delay to plot (range of histogram)
    :param resolution: Time resolution (bin width of histogram)
    :param show_poisson: If true, show poison like distribution
    :param log: If true, use logarithmic scaling
    :param swap_axis: If True, swap plot axis
    :param plot_kwargs: Keyword arguments to be passed to plotting method
    """
    nbins = int(np.ceil(limit/resolution))
    delay = spill_data.hitdelay
    hist = c, t = np.histogram(delay, range=(0, resolution*nbins), bins=nbins) #, weights=np.ones_like(delay)/resolution)
    ax.stairs(*hist, orientation='horizontal' if swap_axis else 'vertical', **plot_kwargs)
    ax_set(ax, swap_axis,
           xlabel='Delay between consecutive particles / s', xlim=(resolution, resolution*nbins), xscale='log' if log else None,
           ylabel=f'Particle count / ${(resolution*SI.s).to_compact():~gL}$ delay interval', yscale='log' if log else None)

    if show_poisson:
        # plot poisson statistics (exponential distribution)
        l = spill_data.counts/spill_data.length
        p = np.exp(-l*t[:-1]) - np.exp(-l*t[1:]) # probability of delay in between t[i] and t[i+1]
        c = p*spill_data.counts
        c[c<1] = 0 # supress fractional counts
        ax.stairs(c, t, orientation='horizontal' if swap_axis else 'vertical', color='gray', label='Poisson statistics', zorder=-1, lw=1)




def plot_spill_consecutive_particle_delay_2D(ax, spill_data, nseg, limit=5e-6, resolution=10e-9, log=True, colorbar=False, **plot_kwargs):
    """Plot histogram of consecutive particle separation (particle-interval histograms) over extraction time
    
    :param ax: Axis to plot onto
    :param spill_data: Instance of TDCSpill
    :param nseg: Number of time segments to divide spill into
    :param limit: Maximum delay to plot (range of histogram)
    :param resolution: Time resolution (bin width of histogram)
    :param log: If true, use logarithmic scaling
    :param colorbar: If true, add a colorbar. To control the location of the colorbar, use colorbar=axis
    :param plot_kwargs: Keyword arguments to be passed to plotting method
    """
    
    nbins = int(np.ceil(limit/resolution))
    counts = np.zeros((nseg, nbins))
    times = spill_data.time_offset + np.linspace(0, spill_data.length, nseg+1)
    
    for i in range(nseg):
        delay = spill_data.segment(times[i], times[i+1]).hitdelay
        counts[i,:], edges = np.histogram(delay, range=(0, resolution*nbins), bins=nbins)
    
    if log and 'norm' not in plot_kwargs: plot_kwargs['norm'] = mpl.colors.LogNorm(vmin=1, vmax=None)
    cm = ax.pcolormesh(times, edges, counts.T, **plot_kwargs)
    
    ax.set(ylabel='Delay between consecutive particles / s', ylim=(resolution, resolution*nbins), yscale='log' if log else None,
           xlabel='Extraction time / s')
    if colorbar:
        ax.get_figure().colorbar(cm, label=f'Particle count / ${(resolution*SI.s).to_compact():~gL}$ delay interval',
                                 ax=colorbar if isinstance(colorbar, mpl.axes.Axes) else None)




def plot_spill_frequency_spectrum(ax, spill_data, fmax=100e3, swap_axis=False, log=True, colorbar=False, **plot_kwargs):
    """Plot frequency spectrum of particle arrival times
    
    :param ax: Axis to plot onto
    :param spill_data: Instance of TDCSpill
    :param fmax: Maximum frequency for analysis in Hz
    :param log: If true, use logarithmic scaling
    :param swap_axis: If True, swap plot axis
    :param plot_kwargs: Keyword arguments to be passed to plotting method
    """
    
    # re-sample timestamps into equally sampled time series
    dt = 1/2/fmax
    timeseries, _ = spill_data.hitcounts(dt)
    
    # frequency analysis
    freq = np.fft.rfftfreq(len(timeseries), d=dt)[1:]
    mag = np.abs(np.fft.rfft(timeseries))[1:]
    mag *= 2/len(timeseries) # amplitude
    mag = mag**2 # power
    
    # plot
    plot = (mag, freq) if swap_axis else (freq, mag)
    ax.plot(*plot, **plot_kwargs)
    
    props = dict(xlabel=f'Frequency / Hz', xlim=(0, fmax), 
                 ylabel='Power spectrum / a.u.', ylim=(0, 1.1*max(mag)))
    if log: props.update(dict(xscale='log', xlim=(10, fmax),
                              yscale='log', ylim=(0.1*np.mean(mag), 1.1*max(mag))))
    ax_set(ax, swap_axis, **props)




def plot_spill_frequency_spectrum_2D(ax, spill_data, nseg, fmax=100e3, log=True, colorbar=False, debug_mode=1, **plot_kwargs):
    """Plot frequency spectrum of particle arrival times over extraction time
    
    :param ax: Axis to plot onto
    :param spill_data: Instance of TDCSpill
    :param nseg: Number of time segments to divide spill into
    :param fmax: Maximum frequency for analysis in Hz
    :param log: If true, use logarithmic scaling
    :param colorbar: If true, add a colorbar. To control the location of the colorbar, use colorbar=axis
    :param plot_kwargs: Keyword arguments to be passed to plotting method
    """

    # frequency analysis with STFT
    dt = 1/2/fmax
    timeseries, _ = spill_data.hitcounts(dt)
    freq, times, mags = scipy.signal.stft(timeseries, fs=1/dt, nperseg=timeseries.size/nseg, noverlap=0, window='boxcar', padded=False, scaling='psd')
    mags = np.abs(mags)**2
            
    # plot
    if log and 'norm' not in plot_kwargs: plot_kwargs['norm'] = mpl.colors.LogNorm(vmin=0.1*np.mean(mags), vmax=1.1*np.max(mags))
    cm = ax.pcolormesh(times, freq, mags, **plot_kwargs)
    
    props = dict(xlabel='Extraction time / s',
                 ylabel='Frequency / Hz', ylim=(0, fmax))
    if log: props.update(dict(yscale='log', ylim=(10, fmax)))
    ax.set(**props)
    if colorbar:
        ax.get_figure().colorbar(cm, label=f'Power spectral density / a.u.', ax=colorbar if isinstance(colorbar, mpl.axes.Axes) else None)
        
    
    # # re-sample timestamps into equally sampled time series
    # dt = 1/2/fmax
    # timeseries, _ = spill_data.hitcounts(dt, spill_data.length/nseg)
    # 
    # # frequency analysis (DC supressed)
    # freq = (np.arange(timeseries.shape[1]//2+1)+0.5) / (dt*timeseries.shape[1]) # bin edges (without DC)
    # mags = np.zeros((timeseries.shape[0], freq.size-1))
    # times = spill_data.time_offset + np.linspace(0, spill_data.length, nseg+1) # bin edges
    # for i in range(nseg):
    #     mags[i,:] = np.abs(np.fft.rfft(timeseries[i]))[1:]
    # 
    # # plot
    # if log and 'norm' not in plot_kwargs: plot_kwargs['norm'] = mpl.colors.LogNorm(vmin=0.1*np.mean(mags), vmax=1.1*np.max(mags))
    # cm = ax.pcolormesh(times, freq, mags.T, **plot_kwargs)
    # 
    # props = dict(xlabel='Extraction time / s',
    #              ylabel='Frequency / Hz', ylim=(0, fmax))
    # if log: props.update(dict(yscale='log', ylim=(10, fmax)))
    # ax.set(**props)
    # if colorbar:
    #     ax.get_figure().colorbar(cm, label=f'Magnitude / a.u.', ax=colorbar if isinstance(colorbar, mpl.axes.Axes) else None)

    
    
def plot_spill_rf_correlation(ax, spill, segments=None, *, phase=True, offset=0, nbins=100, cumulative=0, swap_axis=False, colorbar=None, **plot_kwargs):
    """Plot 1D or 2D histogram of particle arrival time with respect to RF phase
    
    :param ax: Axis to plot onto
    :param spill: Instance of TDCSpill
    :param segments: If not None, make a 2D color plot with segments (time slices along spill) as specified.
                     Can be an integer number n or a tuple (n, t0, t1) with number of segments n in the time interval t0 to t1.
    :param phase: use RF phase in rad if true, RF period in s otherwise
    :param offset: RF offset (in s or rad) to add
    :param nbins: number of bins for histogram
    :param cumulative: If True, plot cumulative histogram. For a 2D color plot you can also pass a tuple (along_spill_time, along_rf_phase)
    :param swap_axis: If True, swap plot axis
    :param colorbar: If set, add a colorbar. If this is a dict, it is passed as keyword parameters to the colorbar function
    :param plot_kwargs: Keyword arguments to be passed to plotting method
    """
    plot_1d = segments is None
    if plot_1d: segments = 1
    if type(segments) is int: segments = (segments,)
    if not hasattr(cumulative, '__len__'): cumulative = [True, cumulative] if plot_1d else [cumulative, False]
    density_in_time, density_in_modrf = not cumulative[0], not cumulative[1]
    
    
    # Calculate histogram(s)
    nseg = segments[0]
    hist = np.zeros((nseg, nbins))
    spills = spill.segments(*segments)
    times = np.zeros(nseg+1)
    
    for i, (t, s) in enumerate(spills):
        times[i:i+2] = t
        hist[i,:], edges = np.histogram(s.hittimes_rf(phase=phase, offset=offset), bins=nbins, density=density_in_modrf)
        np.nan_to_num(hist[i,:], False)
        if density_in_modrf: hist[i,:] *= s.counts/spill.counts
        if density_in_time: hist[i,:] /= np.diff(t)
    
    if cumulative[1]: hist = np.cumsum(hist, axis=1)
    if cumulative[0]: hist = np.cumsum(hist, axis=0)
    
    # Plot
    ylabel = 'Particles'
    if density_in_modrf: ylabel += ' / ' + ('rad' if phase else 'µs')
    if density_in_time: ylabel += ' / s'
    xlabel='Particle arrival time modulo RF'
    if phase:
        xlabel += ' phase / rad'
    else:
        edges *= 1e6
        xlabel += ' period / µs'
    
    if plot_1d:
        # 1D plot
        ax.stairs(hist[0,:], edges, baseline=None if cumulative else 0, orientation='horizontal' if swap_axis else 'vertical', **plot_kwargs)
        ax_set(ax, swap_axis, ylabel=ylabel)
        
    else:
        # 2D color plot
        cm = ax.pcolormesh(*((times, edges, hist.T) if swap_axis else (edges, times, hist)), **dict(dict(cmap='gist_heat_r'), **plot_kwargs))
        ax_set(ax, swap_axis, ylabel='Extraction time / s', ylim=(np.min(times), np.max(times)))
        if colorbar:
            ax.get_figure().colorbar(cm, label=ylabel, **(colorbar if type(colorbar) is dict else {}))
    
    ax_set(ax, swap_axis, xlabel=xlabel, xlim=(0, 2*np.pi if phase else np.max(edges)))
    if phase: format_axis_radians(ax.yaxis if swap_axis else ax.xaxis)

