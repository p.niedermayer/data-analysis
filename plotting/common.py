import scipy
import scipy.signal
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import pint

from ..data import *
from ..fitting import *


# Setup pint
SI = pint.UnitRegistry()
SI.setup_matplotlib()
SI.default_format = '~P'


# Colors
# https://arxiv.org/abs/2107.02270
petroff_colors = ["#3f90da", "#ffa90e", "#bd1f01", "#94a4a2", "#832db6", "#a96b59", "#e76300", "#b9ac70", "#717581", "#92dadd"]
cmap_petroff_10 = mpl.colors.ListedColormap(petroff_colors, 'Petroff 10')
mpl.rcParams['axes.prop_cycle'] = mpl.cycler(color=petroff_colors)

cmap_petroff_gradient = mpl.colors.LinearSegmentedColormap.from_list('Petroff gradient', [petroff_colors[i] for i in (9,0,4,2,6,1)])
cmap_petroff_gradient.set_under(petroff_colors[3])
cmap_petroff_gradient.set_over(petroff_colors[7])
#mpl.rcParams['image.cmap'] = cmap_petroff_gradient

cmap_petroff_bipolar = mpl.colors.LinearSegmentedColormap.from_list('Petroff bipolar', [petroff_colors[i] for i in (2,6,1,3,9,0,4)])
cmap_petroff_bipolar.set_under(petroff_colors[5])
cmap_petroff_bipolar.set_over(petroff_colors[8])


# Matplotlib options
mpl.rcParams.update({
    'figure.figsize': (8, 5),  # Note: overwritten when using the %matplotlib magics
    'figure.dpi': 120,         # Note: overwritten when using the %matplotlib magics
    'figure.constrained_layout.use': True,
    'legend.fontsize': 'x-small',
    'legend.title_fontsize': 'small',
    'grid.color': '#DDD',
})




# Utility methods



def ax_set(ax, swap_xy=False, **kwargs):
    if swap_xy:
        nkwargs = {}
        for k in kwargs:
            nk = (('y' if k[0]=='x' else 'x') + k[1:]) if k[0] in 'xy' else k
            nkwargs[nk] = kwargs[k]
        ax.set(**nkwargs)
    else:
        ax.set(**kwargs)


        
def plot_harmonics(ax, f, df=0, *, n=20, time=False, swap_axis=False, **plot_kwargs):
    """Add vertical lines or spans indicating the location of the frequencies / frequency bands and their harmonics
    
    :param ax: Axis to plot onto
    :param f: Frequency or list of frequencies
    :param df: Bandwidth or list of bandwidths
    :param n: Number of harmonics to plot
    :param time: If true, plot multiples of period T=1/f
    :param swap_axis: If True, swap plot axis (i.e. plot horizontal spans)
    :param plot_kwargs: Keyword arguments to be passed to plotting method
    """
    if not hasattr(f, '__iter__'): f = [f]
    if not hasattr(df, '__iter__'): df = [df]*len(f)
    #kwargs = dict(zorder=-1, color='gray', alpha=0.5, lw=1)
    kwargs = dict(zorder=-1, color='gray', lw=1)
    kwargs.update(plot_kwargs)
    for i in range(1, n+1):
        for j, (fi, dfi) in enumerate(zip(f, df)):
            if dfi == 0:
                method = (ax.axhline if swap_axis else ax.axvline)
                args = [i/fi] if time else [i*fi]
            else:
                method = (ax.axhspan if swap_axis else ax.axvspan)
                args = [i/(fi+dfi/2), i/(fi-dfi/2)] if time else [i*(fi-dfi/2), i*(fi+dfi/2)]
            #method(*args, **kwargs)
            method(*args, **dict(dict(alpha=1-np.log(1+(np.e-1)*(i-1)/n)), **kwargs))
            kwargs.pop('label', None)

def plot_revolution_harmonics(ax, f0, **kwargs):
    """See plot_harmonics
    """
    plot_harmonics(ax, f0, 0, **dict(dict(label='Revolution harmonics', color='lightgray', alpha=1, zorder=-3, ls=':'), **kwargs))

def plot_tune_harmonics(ax, f0, q, dq=0, *, n=20, time=False, swap_axis=False, **plot_kwargs):
    """Add vertical lines or spans indicating the location of the frequencies / frequency bands and their harmonics
    
    :param ax: Axis to plot onto
    :param f0: Revolution frequency to which the tune parameters refer
    :param q: Tune or list of tunes
    :param dq: Bandwidth or list of bandwidths in tune units
    :param n: Number of harmonics to plot
    :param time: If true, plot multiples of period T=1/f
    :param swap_axis: If True, swap plot axis (i.e. plot horizontal spans)
    :param plot_kwargs: Keyword arguments to be passed to plotting method
    """
    if not hasattr(q, '__iter__'): q = [q]
    if not hasattr(dq, '__iter__'): dq = [dq]*len(q)
    plot_harmonics(ax, f0*np.array(q), f0*np.array(dq), n=n, time=time, swap_axis=swap_axis, **plot_kwargs)

def plot_excitation_harmonics(ax, f0, q, dq, **kwargs):
    """See plot_tune_harmonics
    """
    plot_tune_harmonics(ax, f0, q, dq, **dict(dict(label='Excitation harmonics', ls='--', color='k'), **kwargs))

def plot_3rd_order_harmonics(ax, f0, **kwargs):
    """See plot_tune_harmonics
    """
    plot_tune_harmonics(ax, f0, 1/3, 0, **dict(dict(label='1/3 resonance harmonics', color='red', zorder=-2), **kwargs))

