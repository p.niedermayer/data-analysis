import scipy
import scipy.odr
import numpy as np


# Fitting
def fit_linear(S, V, **kwargs):
    """Fit a linear function to the data
    
    :param S: data x values
    :param V: data y values
    :param xerr: data x uncertainties
    :param yerr: data y uncertainties
    :param p0: initial values of parameters
    :param odr: Fit method to use: True means ortogonal distance regression (ODR), False means ordinary least square fit, None (default) uses ODR only if either xerr or yerr is not None
    :param extend: fraction by which the returned fit trace extends beyond the first/last data point
    """
    result = fit_function(lambda s, v0, m: v0+m*s, S, V, p0=(np.mean(V), np.mean(np.diff(V)/np.diff(S))), **kwargs)
    param, param_error, function, [X, _] = result
    return param, param_error, function, [X, function(X, *param)]

def fit_gaussian(S, V, **kwargs):
    """Fit a gaussian function to the data
    
    :param S: data x values
    :param V: data y values
    :param xerr: data x uncertainties
    :param yerr: data y uncertainties
    :param p0: initial values of parameters
    :param odr: Fit method to use: True means ortogonal distance regression (ODR), False means ordinary least square fit, None (default) uses ODR only if either xerr or yerr is not None
    :param extend: fraction by which the returned fit trace extends beyond the first/last data point
    """
    gauss = lambda s, v0, vp, s0, sigma: v0+vp*np.exp(-0.5*((s-s0)/sigma)**2)
    v0 = np.min(V)
    vp = np.max(V)-v0
    s0 = S[np.argmax(V)]
    sigma = np.sqrt(np.abs(np.sum((S - s0) ** 2 * V) / np.sum(V)))
    result = fit_function(gauss, S, V, p0=(v0, vp, s0, sigma), **kwargs)
    param, param_error, function, [X, _] = result
    param[3] = np.abs(param[3]) # return the positive sigma (could use bounds instead, but that's more likely to fail)
    return param, param_error, function, [X, function(X, *param)]

def fit_lorenzian(S, V, **kwargs):
    """Fit a lorenzian function to the data
    
    :param S: data x values
    :param V: data y values
    :param xerr: data x uncertainties
    :param yerr: data y uncertainties
    :param p0: initial values of parameters
    :param odr: Fit method to use: True means ortogonal distance regression (ODR), False means ordinary least square fit, None (default) uses ODR only if either xerr or yerr is not None
    :param extend: fraction by which the returned fit trace extends beyond the first/last data point
    """
    lorenzian = lambda s, v0, vp, s0, gamma: v0 + vp/(1+((s-s0)/gamma)**2)
    v0 = np.min(V)
    vp = np.max(V)-v0
    s0 = S[np.argmax(V)]
    sigma = np.sqrt(np.abs(np.sum((S - s0) ** 2 * (V-v0)) / np.sum(V-v0)))
    result = fit_function(lorenzian, S, V, p0=(v0, vp, s0, sigma), **kwargs)
    param, param_error, function, [X, _] = result
    param[3] = np.abs(param[3]) # return the positive sigma (could use bounds instead, but that's more likely to fail)
    return param, param_error, function, [X, function(X, *param)]


def fit_multi_lorenzian(S, V, n=2, *, s0=None, dry_run=False, **kwargs):
    """Fit multiple lorenzian peaks to the data
    
    :param S: data x values
    :param V: data y values
    :param n: number of peaks to fit. If s0 is given, this is overwritten by len(s0)
    :param xerr: data x uncertainties
    :param yerr: data y uncertainties
    :param s0: initial peak positions
    :param p0: initial values of parameters
    :param odr: Fit method to use: True means ortogonal distance regression (ODR), False means ordinary least square fit, None (default) uses ODR only if either xerr or yerr is not None
    :param extend: fraction by which the returned fit trace extends beyond the first/last data point
    """
    
    def multi_lorenzian(s, v0, *vp_s0_gamma):
        v = v0*np.ones_like(s)
        for vp, s0, gamma in np.reshape(vp_s0_gamma, (-1, 3)):
            v += vp/(1+((s-s0)/gamma)**2)
        return v
    
    # ! for multi peak fit, initial conditions are very important
    if s0 is None:
        V0 = np.copy(V)
        p0 = [0]
        for i in range(n):
            pi, _, function, _ = fit_lorenzian(S, V0, **kwargs)
            p0[0] += pi[0]
            p0.extend(pi[1:])
            V0 -= function(S, *pi)
    else:
        v0 = np.min(V)
        vp = np.max(V)-v0
        p0=[v0]
        for s in s0:
            sigma = np.sqrt(np.abs(np.sum((S - s) ** 2 * (V-v0)) / np.sum(V-v0)))/10
            p0.extend([vp, s, sigma])
    
    if dry_run:
        mi, ma = min(S), max(S)
        X = np.linspace(mi, ma, 1000)
        print('p0:', p0)
        return p0, np.zeros_like(p0), 0, [X, multi_lorenzian(X, *p0)]
        
    result = fit_function(multi_lorenzian, S, V, p0=p0, **kwargs)
    param, param_error, function, [X, _] = result
    param[3] = np.abs(param[3]) # return the positive sigma (could use bounds instead, but that's more likely to fail)
    return param, param_error, function, [X, function(X, *param)]



def fit_moving_gaussian(S, T, VV, extend=0, **kwargs):
    """Fit a gaussian function with linearly moving centroid position to the 2D data
    
    :param S: data x values
    :param T: data y values (axis along which centroid varies)
    :param VV: data z values of shape [x,y]
    :param p0: initial values of parameters
    :param odr: Fit method to use: True means ortogonal distance regression (ODR), False means ordinary least square fit, None (default) uses ODR only if either xerr or yerr is not None
    :param extend: fraction by which the returned fit trace extends beyond the first/last data point
    """
    
    #r = np.repeat([np.linspace(0, 1, m)], n, axis=0).T
    ST = np.tile(S, (len(T), 1)).T
    r = np.linspace(0, 1, ST.shape[1]).reshape(1, -1)
    moving_gauss = lambda s, v0, vp, s0, ds, sigma: v0+vp*np.exp(-0.5*((s-(s0 + ds*r))/sigma)**2)

    V0 = np.average(VV[:, :VV.shape[1]//5], axis=1)  # average over first 5% of data for initial mean
    V1 = np.average(VV[:, -VV.shape[1]//5:], axis=1)  # average over last 5% of data for final mean
    v0 = np.min(VV)
    vp = np.max(VV)-v0
    s0 = S[np.argmax(V0)]
    s1 = S[np.argmax(V1)]
    ds = s1 - s0
    sigma = np.sqrt(np.abs(np.sum((S - s0) ** 2 * V0) / np.sum(V0)))
    result = fit_function(lambda *a, **b: moving_gauss(*a, **b).ravel(), ST, VV.ravel(), p0=(v0, vp, s0, ds, sigma), extend=None, **kwargs) # flatten 2D arrays for fit
    param, param_error, function, _ = result
    param[4] = np.abs(param[4]) # return the positive sigma (could use bounds instead, but that's more likely to fail)
    mi, ma = np.min(T), np.max(T)
    X = np.linspace(mi - (ma-mi)*extend, ma + (ma-mi)*extend, 1000)
    Y = param[2] + param[3]*np.linspace(0, 1, 1000)
    return param, param_error, function, [X, Y]
    

def fit_function(function, x, y, *, xerr=None, yerr=None, p0=None, odr=None, extend=0):
    """Fit a function to the data
    
    :param function: fit function(x, *param)
    :param x: data x values
    :param y: data y values
    :param xerr: data x uncertainties
    :param yerr: data y uncertainties
    :param p0: initial values of parameters
    :param odr: Fit method to use: True means ortogonal distance regression (ODR), False means ordinary least square fit, None (default) uses ODR only if either xerr or yerr is not None
    :param extend: fraction by which the returned fit trace extends beyond the first/last data point
    """
    if odr is None:
        odr = xerr is not None or yerr is not None
    
    if odr:
        # orthogonal distance regression
        data = scipy.odr.RealData(x, y, sx=xerr, sy=yerr)
        model = scipy.odr.Model(lambda beta, x: function(x, *beta))
        odr = scipy.odr.ODR(data, model, p0)
        # if not odr: odr.set_job(fit_type=2) # ordinary least-squares
        output = odr.run()
        param, param_error = output.beta, output.sd_beta
    else:
        # non-linear least squares
        param, cov = scipy.optimize.curve_fit(function, x, y, p0)
        param_error = np.sqrt(np.abs(cov.diagonal()))
    
    if extend is not None:
        mi, ma = np.min(x), np.max(x)
        X = np.linspace(mi - (ma-mi)*extend, ma + (ma-mi)*extend, 1000)
        return param, param_error, function, [X, function(X, *param)]
    else:
        return param, param_error, function, None

def fit_exponential(S, V, **kwargs):
    """Fit an exponential function to the data
    
    :param S: data x values
    :param V: data y values
    :param xerr: data x uncertainties
    :param yerr: data y uncertainties
    :param p0: initial values of parameters
    :param odr: Fit method to use: True means ortogonal distance regression (ODR), False means ordinary least square fit, None (default) uses ODR only if either xerr or yerr is not None
    :param extend: fraction by which the returned fit trace extends beyond the first/last data point
    """
    exponential = lambda s, v0, vp, s0: v0 + vp*np.exp(s/s0)
    v0 = np.min(V)
    vp = np.max(V)-v0
    s0 = 1
    return fit_function(exponential, S, V, p0=(v0, vp, s0), **kwargs)

