#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = "Philipp Niedermayer"
__contact__ = "p.niedermayer@gsi.de"


import re
from pint import Quantity as Qty
c_light = Qty('c')


elements = {int(a.split("=")[0]): a.split("=")[1] for a in (
        "1=H,2=He,3=Li,4=Be,5=B,6=C,7=N,8=O,9=F,10=Ne,11=Na,12=Mg,13=Al,14=Si,15=P,16=S,17=Cl,18=Ar,19=K,20=Ca,"
        "21=Sc,22=Ti,23=V,24=Cr,25=Mn,26=Fe,27=Co,28=Ni,29=Cu,30=Zn,31=Ga,32=Ge,33=As,34=Se,35=Br,36=Kr,37=Rb,38=Sr,39=Y,40=Zr,"
        "41=Nb,42=Mo,43=Tc,44=Ru,45=Rh,46=Pd,47=Ag,48=Cd,49=In,50=Sn,51=Sb,52=Te,53=I,54=Xe,55=Cs,56=Ba,57=La,58=Ce,59=Pr,60=Nd,"
        "61=Pm,62=Sm,63=Eu,64=Gd,65=Tb,66=Dy,67=Ho,68=Er,69=Tm,70=Yb,71=Lu,72=Hf,73=Ta,74=W,75=Re,76=Os,77=Ir,78=Pt,79=Au,80=Hg,"
        "81=Tl,82=Pb,83=Bi,84=Po,85=At,86=Rn,87=Fr,88=Ra,89=Ac,90=Th,91=Pa,92=U,93=Np,94=Pu,95=Am,96=Cm,97=Bk,98=Cf,99=Es,100=Fm,"
        "101=Md,102=No,103=Lr,104=Rf,105=Db,106=Sg,107=Bh,108=Hs,109=Mt,110=Ds,111=Rg,112=Cn,113=Nh,114=Fl,115=Mc,116=Lv,117=Ts,118=Og"
    ).split(",")}

class Ion:
    """Class representing an ion

    Examples:
        Ion("Proton @1GeV/c")
        Ion("1H1+ @1GeV/c")
        Ion("14N7+ @200MeV/u")
        Ion("N", "7e", "14u", "200MeV/u")
        Ion(q="7e", m="14u", Ekin_per_u="200MeV/u")
        Ion("N", "7e", "14u", "200MeV/u")

    Args:        
        name (str): Name of Ion. If this is a well-defined name like `14 N 7+`, mass, charge and atomic number are recognized.
              May optionally include an energy, momentum or rigidity specification after an @ sign
        q (str | pint.Quantity): Charge with unit, e.g. `1e`
        m (str | pint.Quantity): Rest mass with unit, e.g. `14u`
        Ekin_per_u (str | pint.Quantity): Specific kinetic energy, e.g. `200MeV/u`
        p (str | pint.Quantity): Momentum, e.g. `1GeV/c`
        E (str | pint.Quantity): Total energy, e.g. `1GeV`
        Ekin (str | pint.Quantity): Kinetic energy, e.g. `50MeV`
        Brho (str | pint.Quantity): Magnetic rigidity, e.g. `5 T m`
        Z (int): Atomic number (nuclear charge number, only if name is not given)
        
        
    """
    def __init__(self, name=None, q=None, m=None, Ekin_per_u=None, *, p=None, E=None, Ekin=None, Brho=None, Z=None):
        known_name = None if name is None else name.replace('Proton','1H1+').replace('Deuterium','2H1+')
        if q is None and m is None and (m := re.match(f"(\d+)\s*([A-Za-z]+)\s*(\d+)([ +-])\s*(@.*)?", known_name)):
            mass, name, charge, sign, energy = m.groups()
            m, q = Qty(int(mass), "u"), Qty(int(charge), "e")
            if energy:
                value = Qty(energy[1:].strip())
                if value.is_compatible_with('eV/u'):
                    Ekin_per_u = value
                elif value.is_compatible_with('eV/c'):
                    p = value
                elif value.is_compatible_with('tesla meter'):
                    Brho = value
                else:
                    raise ValueError(f"{energy} given but cannot be identified as Ekin_per_u, p or Brho")
            if sign == "-": q = -q
        self.q = Qty(q).to('e')
        self.m = Qty(m).to('u')
        self.Z = Z
        if name is None and Z is not None:
            name = elements[Z]
        self.name = name
        if self.name in elements.values():
            self.Z = list(elements.keys())[list(elements.values()).index(self.name)]
        if [Ekin_per_u, p, E, Ekin, Brho].count(None) != 4:
            raise ValueError("Exactly one of p, Brho, E, Ekin or Ekin_per_u must be given")
        if Brho is not None:
            p = Qty(Brho) * self.q
        if p is not None:
            self.p = Qty(p)
        if Ekin_per_u is not None:
            Ekin = Qty(Ekin_per_u) * self.m
        if Ekin is not None:
            E = self.E0 + Qty(Ekin)
        if E is not None:
            self.p = ( Qty(E)**2 - self.E0**2 )**0.5 / c_light
        self.p = self.p.to('MeV/c')

    
    @property
    def A(self):
        """Nucleon number (atomic mass number)"""
        return round(self.m.m_as('u'))

    @property
    def N(self):
        """Neutron number"""
        return self.A - self.Z
    
    @property
    def E0(self):
        """Rest energy"""
        return (self.m*c_light**2).to('MeV')

    @property
    def E(self):
        """Total energy"""
        return (( (self.p*c_light)**2 + self.E0**2 )**.5).to('MeV')

    @property
    def Ekin(self):
        """Kinetic energy"""
        return self.E - self.E0

    @property
    def Ekin_per_u(self):
        """Specific kinetic energy"""
        return (self.Ekin/self.m).to('MeV/u')

    @property
    def beta(self):
        """Relativistic beta"""
        return self.p*c_light/self.E
 
    @property
    def v(self):
        """Speed"""
        return self.beta * c_light
 
    @property
    def gamma(self):
        """Relativistic lorentz factor gamma"""
        return self.E/self.E0
  
    @property
    def Brho(self):
        """Magnetic rigidity"""
        return (self.p/self.q).to('T*m')
  
    @property
    def Erho(self):
        """Electric rigidity"""
        return (self.p*self.v/self.q).to('GV')

    @property
    def element(self):
        """Element symbol"""
        return elements.get(self.Z)
    
    @property
    def symbol(self):
        """Ion symbol"""
        charge = self.q.m_as('e')
        return f"{self.A}{self.element or '?'}{abs(charge)}{'+' if charge>0 else '-'}" if self.element else None
    
    def __repr__(self):
        return (f'Ion({self.name}, q={self.q:~Pg}, m={self.m:~Pg}, p={self.p:~Pg}, Ekin={self.Ekin_per_u:~Pg},\n'
                f'    beta={self.beta:~Pg}, gamma={self.gamma:~Pg}, Brho={self.Brho:~Pg}, Erho={self.Erho:~Pg})')

