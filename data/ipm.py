#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Dataclasses for IPM data

Classes to work with data from old ionisation profile monitor application.

"""

__author__ = "Philipp Niedermayer"
__contact__ = "p.niedermayer@gsi.de"
__date__ = "2022-05-17"




import numpy as np
from dataclasses import dataclass, field
from .common import Trace
import tarfile
import re



@dataclass
class IPMData(Trace):
    """Container for profile data from IPM
    
    t - time array in seconds
    w - profile width array
    w_unit - unit of displacement array values
    x - horizontal profile amplitudes
    y - vertical profile amplitudes
    unit - unit of profile amplitudes
    hf - HF frequency (h*f_rev) in Hz
    """
    t: np.ndarray
    w: np.ndarray
    x: np.ndarray
    y: np.ndarray
    unit: str = 'a.u.'
    w_unit: str = 'mm'
    x_rms: np.ndarray = None
    y_rms: np.ndarray = None
    beam_current: np.ndarray = None
    dipole: np.ndarray = None
    hf: np.ndarray = None
    events: dict = field(default_factory=dict)
    
    
    def subtract_background(self, time_range):
        """Subtract the average profile measured in the given timespan
        
        Note that this does not affect rms beam sizes
        
        :param time_range: tuple of (start, end) of calibration window in seconds
        """
        window = irng(self.t, *time_range)
        self.x -= np.mean(self.x[window], axis=0)
        self.y -= np.mean(self.y[window], axis=0)
    
    
    
    
    @classmethod
    def from_file(cls, fname, *, clean=False, subtract_background=True, from_event=None, time_offset=0, verbose=0):
        """Read in profile data from a *.tgz file saved with IPM application
           
        :param fname: path to filename
        :param clean: if True, use calibrated data from adc_clean.dat instead of raw data from adc.dat
        :param subtract_background: if True, subtract background level prior to injection
        :param from_event: return data from this event on, time will also be relative to this event
        :param time_offset: adds a given time offset (in s) to the timestamps
        
        References: 
            Giacomini, Tino <T.Giacomini@gsi.de>
                One beam profile consist of 64 values.
                The distance between two wires is 0.6 mm. The wires diameter is 1.5 mm. So the center to center distance of the wires is 2.1 mm.
            Handbook RGM_SIS
                At SIS cyclus start event 32 appears. This event is read by the RGM and the measure cyclus starts, see Measure cyclus.
                Mit dem Start eines SIS Zyklus wird auch der RGM gestartet und nimmt Daten auf. Bei Auftreten eines vorher festgelegten Events, speichert die Software die gerade aktuelle Profilnummer und die seit dem Spillstart vergangene Zeit in ms, sog. Time Stamp Function. 
                Es wird mit einer Periode von 10ms ein Strahlprofil aufgenommen.
        
        """
        tar = tarfile.open(fname, "r:gz")
        
        # profile data
        f = tar.extractfile('adc_clean.dat' if clean else 'adc.dat')
        dat = np.loadtxt(f, skiprows=1)
        p = 2.1 * (np.arange(64)-31.5)
        n = dat.shape[0]//64*64
        if dat.shape[0]%64 != 0: print(f'WARNING: loaded incomplete data from {fname}')
        x = dat[:n,1].reshape((-1, 64))
        y = dat[:n,2].reshape((-1, 64))
        t = np.arange(x.shape[0]) * 10e-3 # Es wird mit einer Periode von 10ms ein Strahlprofil aufgenommen.
        
        # time data              
        dat = np.loadtxt(tar.extractfile('bwrms.dat'), skiprows=1)
        _, x_rms, y_rms = dat.T

        # scalar data
        #   File scl.dat contains the scalerdata. 
        #   Col 0 = internal timing,
        #   Col 1 = DC (Current),
        #   Col 2 = Dipole signal,
        #   Col 3 = HF signal,
        #   Col 4 = Ev40 (MB Trigger)  / Injection
        #   Col 5 = Ev45 (Flattop) / Accel end
        #   Col 6 = Ev51 (Extraction End) 
        #   Col 7 = EvXX see GUI->Parameter->Event 
        scl = np.loadtxt(tar.extractfile('scl.dat'), skiprows=1)
        index, beam_current, dipole, hf, event_40, event_45, event_51, event_user = scl.T
        hf /= 10e-3 # Es wird mit einer Periode von 10ms ein Strahlprofil aufgenommen.
        
        # background subtraction
        if subtract_background:
            assert np.max(event_40) == 1, f'Requested background subtraction prior to injection (event 40), but event 40 not found in data'
            nbg = np.argmax(event_40) - 3  # before injection
            x -= np.mean(x[:nbg, :], axis=0)
            y -= np.mean(y[:nbg, :], axis=0)
            if verbose: print(f'Background subtracted (first {nbg} profiles or {t[nbg]} s)')
        
        # events
        events = {}
        if np.max(event_40) == 1:
            events.update({40: np.argmax(event_40)})
        if np.max(event_45) == 1:
            events.update({45: np.argmax(event_45)})
        if np.max(event_51) == 1:
            events.update({51: np.argmax(event_51)})
        if np.max(event_user) == 1:
            # user event
            f = tar.extractfile('param.dat')
            ev = None
            for line in f:
                if verbose: print(line.decode('ascii').strip())
                m = re.match(r'ev\s*=\s*(\d+)', line.decode('ascii').strip())
                if m:
                    ev = int(m.group(1))
            events.update({ev: np.argmax(event_user)})
        
        # offset relative to from_event
        offset = events.get(from_event, 0)
        if offset == 0 and from_event is not None:
            raise RuntimeError(f'Requested data from event {from_event}, but data contains only events {events}')
        
        if verbose: print(f'Returning data with offset {offset} as requested by event {from_event}')
        events = {e: t[o] - t[offset] for e,o in events.items()}
        x, y, t = x[offset:, :], y[offset:, :], t[offset:] - t[offset]
        x_rms, y_rms, beam_current, dipole, hf = [_[offset:] for _ in (x_rms, y_rms, beam_current, dipole, hf)]
        
        return IPMData(t + time_offset, p, x, y,
                        x_rms = x_rms,
                        y_rms = y_rms,
                        beam_current = np.concatenate((beam_current, [np.nan])) if len(beam_current) < len(t) else beam_current,
                        dipole = np.concatenate((dipole, [np.nan])) if len(dipole) < len(t) else dipole,
                        hf = np.concatenate((hf,  [np.nan])) if len(hf) < len(t) else hf,
                        events = events,
                       )

