#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Dataclasses for NWA data

Classes to work with data from vector network analyser

"""

__author__ = "Philipp Niedermayer"
__contact__ = "p.niedermayer@gsi.de"
__date__ = "2022-05-17"




import numpy as np
from dataclasses import dataclass
from .common import Trace




@dataclass
class NWAData(Trace):
    """Container for data from Network Analyser save
    
    f - frequency in Hz
    m - magnitude
    p - phase
    """
    f: np.ndarray
    m: np.ndarray
    p: np.ndarray
    f_unit: str = 'Hz'
    m_unit: str = 'a.u.'
    p_unit: str = 'a.u.'
    
    @classmethod
    def from_file(cls, fname, phasefile=None, *, isdeg=True, asdeg=None, unwrap=False, S='21', verbose=0):
        """Read in data from CSV or S2P file for magnitude and phase
        
        :param fname: path to filename
        :param phasefile: path to filename with phase trace data if not included in fname file
        :param unwrap: if true, relative phase is unwraped to absolute phase centered around zero
        :param isdeg: if phase data in file is in degree (True) or radians (False)
        :param asdeg: if returned phase data shall be in degree (True) or radians (False) or same as isdeg (None)
        """
        if fname.endswith('.s2p'):
            with open(fname) as f:
                head = f.readlines()[2].strip()
            
            if head == '! Freq magS11 angS11 magS21 angS21 magS12 angS12 magS22 angS22':
                Freq, magS11, angS11, magS21, angS21, magS12, angS12, magS22, angS22 = np.loadtxt(fname, skiprows=4).T
                f, m, p = Freq, locals().get(f'magS{S}'), locals().get(f'angS{S}')
            elif head == '! Freq ReS11 ImS11 ReS21 ImS21 ReS12 ImS12 ReS22 ImS22':
                Freq, ReS11, ImS11, ReS21, ImS21, ReS12, ImS12, ReS22, ImS22 = np.loadtxt(fname, skiprows=4).T
                Z = locals().get(f'ReS{S}') + 1j*locals().get(f'ImS{S}')
                f, m, p = Freq, np.abs(Z), np.angle(Z)
                isdeg = False
            else:
                raise ValueError(f'Unexpected file format: {head}')
            
        else:
            f, m, p, *other = np.loadtxt(fname, skiprows=3, delimiter=',').T
            if phasefile is not None:
                fp, p, *_ = np.loadtxt(phasefile, skiprows=3, delimiter=',').T
                if np.any(f != fp):
                    raise ValueError('Files provided do not have equal frequency components.')
        
        
        if unwrap:
            if isdeg: p = np.deg2rad(p)
            p = np.unwrap(p)
            #p -= np.mean(p)
            if isdeg: p = np.rad2deg(p)
        
        p_unit = 'deg' if isdeg else 'rad'
        
        if asdeg is not None:
            if asdeg and not isdeg:
                p = np.rad2deg(p)
                p_unit = 'deg'
            if isdeg and not asdeg:
                p = np.deg2rad(p)
                p_unit = 'rad'            
        
        return NWAData(f, m, p, p_unit=p_unit)


@dataclass
class NWADataAverage(Trace):
    """Container for average data from multiple Network Analyser saves
    
    f - frequency in Hz
    m - magnitude
    p - phase
    """
    f: np.ndarray
    m: np.ndarray
    m_std: np.ndarray
    p: np.ndarray
    p_std: np.ndarray
    f_unit: str = 'Hz'
    m_unit: str = 'a.u.'
    p_unit: str = 'a.u.'
    
    @classmethod
    def from_files(cls, fnames, phasefiles=None, **kwargs):
        """Read data from multiple files and average
        
        See NWAData.from_file for kwargs
        """
        data = [NWAData.from_file(fnames[i], None if phasefiles is None else phasefiles[i], unwrap=True, **kwargs)
                for i in range(len(fnames))]
        assert np.all([np.all(data[0].f == d.f) for d in data]), 'Data points not equal in frequency!'
        assert np.all([data[0].f_unit == d.f_unit for d in data]), 'Data does not have equal units of frequency!'
        assert np.all([data[0].m_unit == d.m_unit for d in data]), 'Data does not have equal units of magnitude!'
        assert np.all([data[0].p_unit == d.p_unit for d in data]), 'Data does not have equal units of phase!'
        m = [d.m for d in data]
        p = [d.p for d in data]
        return NWADataAverage(data[0].f,
                              m=np.mean(m, axis=0), m_std=np.std(m, axis=0),
                              p=np.mean(p, axis=0), p_std=np.std(p, axis=0),
                              f_unit=data[0].f_unit, m_unit=data[0].m_unit, p_unit=data[0].p_unit)
            
    