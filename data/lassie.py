#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Dataclasses for Lassie Spill Data

Classes to work with data from lassie application.

"""

__author__ = "Philipp Niedermayer"
__contact__ = "p.niedermayer@gsi.de"
__date__ = "2022-05-17"




import numpy as np
from dataclasses import dataclass, field
from .common import Trace



@dataclass
class LassieSpillData(Trace):
    """Container for time series data from lassie spill or DCCT application
    
    t - time array in seconds
    v - value array in unit
    unit - unit of value array
    events - dict mapping events to time
    """
    t: np.ndarray
    v: np.ndarray
    unit: str = 'particles'
    events: dict = field(default_factory=dict)
    
    @classmethod
    def from_file(cls, fname, from_event=None, time_offset=0, verbose=0):
        """Read in time series data from a *.tdf file saved with lassiespill or DCCT application
           
        :param fname: path to filename
        :param from_event: return data from this event, time will also be relative to this event
        :param time_offset: adds a given time offset (in s) to the timestamps
        
        References: https://git.acc.gsi.de/bi/bdiolib.python
        """
        
        from .bdiolib.bdio import bdio
        b = bdio.BDIOReader(fname)

        appname = ''
        device = ''
        fs = None
        t0 = 0
        t1 = None
        unit = 'particles'
        events = {}
        
        blocks = b.get_directory()
        for bi in blocks:
            b.seek(bi.get_pos())
            block = b.next_block()
            if verbose: print(bi.get_title(), block)
            #if hasattr(block, 'get_rows'):
            #    for row in block.get_rows():
            #        print(row)
            ###############
            if bi.is_header_block():
                appname = block.get_app_name()
                device = block.get_device()
                if verbose: print(device, 'data saved from', appname)
                
            elif bi.get_title() == 'TraceInfo':
                for row in block.get_rows():
                    if verbose: print(row)
                    if row.get_tag() == ('intensity frequency' if appname=='DCCT' else 'sampling frequency'):
                        assert row.get_unit().lower() == 'hz', 'sampling frequency has unexpected unit {}'.format(row.get_unit())
                        fs = row.get_value()
                        
            elif bi.get_title() == 'Integrals':
                for row in block.get_rows():
                    if verbose: print(row)
                    if row.get_tag() == 'Calibrated: beam in integral':
                        unit = row.get_unit()
                        
            elif bi.is_double_array_block() and bi.get_title() == ('Intensity' if appname=='DCCT' else 'Calibrated'):
                values = np.array(block.get_array())
                
            elif bi.is_event_table_block():
                t0 = block.get_rows()[0].get_time() # asume the first event is the start of data # TODO: is there a better way to do it?
                for row in block.get_rows():
                    events[row.get_event()] = (row.get_time() - t0)*1e-9
                                           
                

        
        
        times = np.linspace(0, len(values)/fs, len(values))
        
        if from_event:
            if from_event not in events:
                raise ValueError(f'Requested data from event {from_event}, but event not found in data file')
            from_time = events[from_event]
            idx = np.argmin(np.abs(times - from_time))
            values = values[idx:]
            times = times[idx:] - times[idx]
            events = {e: t - from_time for e,t in events.items()}
        
        return LassieSpillData(times + time_offset, values, unit=unit, events=events)

