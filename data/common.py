#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Common utils for dataclasses



"""

__author__ = "Philipp Niedermayer"
__contact__ = "p.niedermayer@gsi.de"
__date__ = "2022-05-17"




import numpy as np
from dataclasses import dataclass
import scipy.signal




@dataclass
class Trace:
    pass
    

    
    
# timing events
################

EVT_MB_TRIGGER = 40
EVT_FLATTOP = 45
EVT_EXTR_END = 51

CMD_SEQ_START = 257
CMD_BEAM_INJECTION = 283




# util functions
#################


def avg(*data, n=100, function=np.mean):
    """Averages the data by combining n subsequent points into one (works on last axis)
    :param data: the data to average over
    :param n: number of subsequent datapoints of intput to average into one point in the output
    :param function: averaging function to apply to last axis of input data, e.g. np.mean, np.std, np.max etc.
    """
    result = []
    for d in data:
        w = int(d.shape[-1]/n)
        result.append(function(d[...,:w*n].reshape(*d.shape[:-1], w, n), axis=-1))
    return result

def smooth(*data, n):
    return [scipy.signal.savgol_filter(d, n, 0) for d in data] if n else data
    
def irng(array, start, stop):
    """Return a slice indexing the array in the given value range"""
    indices = [np.argmin(np.abs(array - start)), np.argmin(np.abs(array - stop))]
    return slice(min(indices), max(indices))

