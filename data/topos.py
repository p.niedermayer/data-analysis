#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Dataclasses for Libera BPM data

Classes to work with data from libera hadron beam position monitor system

"""

__author__ = "Philipp Niedermayer"
__contact__ = "p.niedermayer@gsi.de"
__date__ = "2022-08-29"




import numpy as np
from dataclasses import dataclass
from .common import Trace




@dataclass
class ToposData(Trace):
    """Container for data from topos
    
    t - time array in seconds
    """
    t: np.ndarray
    x: np.ndarray
    y: np.ndarray
    x_unit: str = 'mm'
    y_unit: str = 'mm'



@dataclass
class ToposBBBData(ToposData):
    """Container for bunch-by-bunch data from topos
    """
    
    @classmethod
    def from_file(cls, fname, time_offset=0, verbose=0):
        """Read in bunch-by-bunch data from a *.bin file saved with topos
        
        :param fname: path to filename
        :param time_offset: adds a given time offset (in s) to the timestamps
        
        """
        reader = BpmBinaryExportReader()
        t, x, y = reader.read(fname)
        return ToposBBBData(t/1e3 + time_offset, x, y)

    def to_tbt_data(self, h, b=None):
        """Returns the turn-by-turn data
        :param h: the harmonic number of the RF system (number of bunches per turn)
        :param b: the bunch index or None for an average
        """
        wrap = lambda a: a[:(len(a)//h)*h].reshape(-1, h).mean(axis=1) if b is None else a[b::h]
        return ToposTBTData(t=wrap(self.t), x=wrap(self.x), y=wrap(self.y),
                            x_unit=self.x_unit, y_unit=self.y_unit)
    
    def __repr__(self):
        return f'ToposBBBData(n_bunch={len(self.t)}, t={self.t[0]:.3f}..{self.t[-1]:.3f} s)'


@dataclass
class ToposTBTData(ToposData):
    """Container for turn-by-turn data from topos
    """
    
    def __repr__(self):
        return f'ToposBBBData(n_turn={len(self.t)}, t={self.t[0]:.3f}..{self.t[-1]:.3f} s)'







#######################################################
#
# Code below thankfully provided by d.vilsmeier@gsi.de
#



import numpy as np
from io import BufferedReader

class _DataIntegrityError(Exception):
    pass

class BpmBinaryExportReader(object):

    encoding = 'utf-8'
    header_separator = b','
    num_header_separators = 4
    peeksize = 45
    datatype = np.dtype('float32')
    numbersize = datatype.itemsize
    num_properties = 3

    @classmethod
    def _detect_header(cls, peeked_bytes):
        occurences = []
        beg = 0
        num_sep_found = 0
        while num_sep_found < cls.num_header_separators:
            index = peeked_bytes.find(cls.header_separator, beg, cls.peeksize)
            if index == -1:
                break
            occurences.append(index)
            num_sep_found += 1
            beg = index + 1
        if len(occurences) < cls.num_header_separators:
            print ("Integrity error")
            raise _DataIntegrityError(
                    f'Peeked bytes contained less than {cls.num_header_separators} separators.'
              )
        header_length = occurences[-1] + 1
        num_points_start = occurences[1] + 1
        num_points_end = occurences[2]
        return header_length, num_points_start, num_points_end

    @classmethod
    def _read_data(cls, num_points, raw_data):
        if num_points == 0: # Have seen files with zero data...
            x = np.array([])
            y = np.array([])
            t = np.array([])
        else:
            a = np.frombuffer(raw_data, dtype=cls.datatype)
            num_floats_expected = cls.num_properties * num_points
            num_floats_read = len(a)
            if num_floats_read != num_floats_expected:
                msg = f'Number of floats read = {num_floats_read} != {num_floats_expected} = number of floats expected'
                raise _DataIntegrityError(msg)
            else:
                a = a.reshape(-1, num_points)
                x = a[0]
                y = a[1]
                t = a[2]
        return t, x, y

    @classmethod
    def read(cls, file):
        with open(file, 'rb') as f:
            r = BufferedReader(f)
            peeked = r.peek(cls.peeksize)
            try:
                header_length, num_points_start, num_points_end = cls._detect_header(peeked)
                header = r.read(header_length).decode(cls.encoding)
                num_points = int(header[num_points_start:num_points_end])
                raw_data = r.read(cls.num_properties * cls.numbersize * num_points)
                data = cls._read_data(num_points, raw_data)
            except _DataIntegrityError as e:
                msg = f"Error reading file '{file}': {e}"
                raise RuntimeError(msg)
            return data
