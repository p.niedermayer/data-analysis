#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Dataclasses for Libera BPM data

Classes to work with data from libera hadron beam position monitor system

"""

__author__ = "Philipp Niedermayer"
__contact__ = "p.niedermayer@gsi.de"
__date__ = "2022-05-17"




import numpy as np
from dataclasses import dataclass
from .common import Trace




@dataclass
class LiberaData(Trace):
    """Container for data from libera-ireg dump
    
    t - time array in seconds
    """
    t: np.ndarray
    x: np.ndarray
    y: np.ndarray
    s: np.ndarray
    x_unit: str = 'mm'
    y_unit: str = 'mm'
    s_unit: str = 'a.u.'



@dataclass
class LiberaBBBData(LiberaData):
    """Container for bunch-by-bunch data from libera-ireg dump
    """
    
    @classmethod
    def from_file(cls, fname, time_offset=0, verbose=0):
        """Read in bunch-by-bunch data from a *.bin file saved with libera-ireg
        
        :param fname: path to filename
        :param time_offset: adds a given time offset (in s) to the timestamps
        
        References: Libera_Hadron_User_Manual_1.04.pdf
        """
        bunch = np.memmap(fname, dtype=np.dtype([
                ('S', '<i4'),
                ('r1', '<i4'),
                ('X', '<i4'),  # in nm
                ('Y', '<i4'),  # in nm
                ('TS', '<u8'), # in 4ns (ADC clock at 250MS/s)
                ('t2', '<u2'),
                ('t1', '<u2'),
                ('status', '<u4')]))
        time = (bunch['TS'] + bunch['t1'])*4e-9 # in s
        return LiberaBBBData(time + time_offset, bunch['X']*1e-6, bunch['Y']*1e-6, bunch['S'])

    def to_tbt_data(self, h, b=0):
        """Returns the turn-by-turn data
        :param h: the harmonic number of the RF system (number of bunches per turn)
        :param b: the bunch index or None for an average over all bunches (note that averaging might wash out betatron oscillations if not in phase)
        """
        wrap = lambda a: a[:(len(a)//h)*h].reshape(-1, h).mean(axis=1) if b is None else a[b::h]
        return LiberaTBTData(t=wrap(self.t), x=wrap(self.x), y=wrap(self.y), s=wrap(self.s),
                             x_unit=self.x_unit, y_unit=self.y_unit, s_unit=self.s_unit)
    


@dataclass
class LiberaTBTData(LiberaData):
    """Container for turn-by-turn data from libera-ireg dump
    """
    pass
