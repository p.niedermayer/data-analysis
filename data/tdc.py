#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Dataclasses for TDC data

Classes to work with spill data from time-to-digital converter (TDC) application
which stores timestamps of particle hits.

"""

__author__ = "Philipp Niedermayer"
__contact__ = "p.niedermayer@gsi.de"
__date__ = "2022-07-28"




import numpy as np
from dataclasses import dataclass


# TDC event data-stream consists of pairs of channel and corresponding timestamps. The following channels exist:
CH_DETECTOR_0 =  0  # Particle hit on detector 0 
CH_DETECTOR_1 =  1  # Particle hit on detector 1
CH_DETECTOR_2 =  2  # Particle hit on detector 2
CH_DETECTOR_3 =  3  # Particle hit on detector 3
CH_DETECTOR_4 =  4  # Particle hit on detector 4
CH_DETECTOR_5 =  5  # Particle hit on detector 5
CH_RF_AUX     =  6  # scaled AUS RF clock tick, one tick every [rfPrescaler] RF cycles (only if rfChannel=6)
CH_RF_DIV20   =  7  # scaled SIS RF clock tick, one tick every 20 RF cycles (only if rfChannel=7)
CH_VIRT_DRF   =  8  # Float32 representing the delta-Rf in TDC-Ticks for reconstruction. Follows every CH_RF_DIV20. Redundant.
CH_CAL_CLK    = 13  # Clock for TDC calibration and precise time slicing
CH_GATE_END   = 14  # Marks end of a spill
CH_GATE_START = 15  # Marks begin of a spill

        
        


@dataclass
class TDCSpill:
    header: np.ndarray
    data: np.ndarray          # from GATE_START to GATE_END (including)
    time_offset: float = 0
    
    @property
    def ch(self):
        return self.data['channel']
    @property
    def ts(self):
        return self.data['timestamp']
    
    def _get_ts(self, channel):
        ts = self.ts[self.ch == channel]
        if np.any(ts[:-1] > ts[1:]): ts.sort()  # Ensure timestamp order (mostly correct, but not always)
        return ts
    
    @property
    def detector(self):
        """Detector name"""
        return self.header['detectorName'].item().decode('ascii')
    
    def _time_till_start(self, ts):
        return self.time_offset + (ts - self.ts[0]) * self.header['tdcTickSecs'].item()
    
    @property
    def length(self):
        """Spill duration in sec"""
        return self._time_till_start(self.ts[-1]) - self.time_offset
    
    @property
    def counts(self):
        """Total particle count"""
        return len(self.hittimes)
    
    @property
    def hittimes(self):
        """Particle arrival times in s with respect to spill start"""
        ts = self._get_ts(self.header['detectorChannel'].item())
        return self._time_till_start(ts)
    
    @property
    def hitdelay(self):
        """Delay between arrival of consecutive particles in s"""
        return np.diff(self.hittimes)
        
    def hitcounts(self, dt, dt2=None):
        """Particle counts per time bin
        
        Re-samples the timestamps into an equally spaced time series with counts per unit time
        :param dt: width in s of time bin for counting.
        :param dt2: width in s of macro time bin. If not None, this function returns a 2D array with time resolution dt2 along the first and dt along the second axis
        :returns: (count_array, bin_edges) in units of (1, second)
        """
        # re-sample timestamps into equally sampled time series of shape [length/dt]
        bins = int(self.length/dt)
        range = (self.time_offset, self.time_offset + bins*dt)
        counts, edges = np.histogram(self.hittimes, bins=bins, range=range)
        
        if dt2 is not None:
            if dt2 <= dt: raise ValueError('Macro time bin dt2 must be greater than micro time bin dt')
            # make 2D array of shape [length/dt2, dt2/dt]
            ebins = int(dt2/dt)
            counts = counts[:int(len(counts)/ebins)*ebins].reshape((-1, ebins))
            edges = edges[:int(len(edges)/ebins+1)*ebins:ebins]
    
        return counts, edges

    def _get_rf(self):
        """Timestamps and prescaler of detected RF signal"""
        ch = self.header['rfChannel'].item()
        rf_ts = self._get_ts(ch)
        if rf_ts.size == 0:
            # RF channel contains no data, try the other channel instead
            print(f"Warning: No RF data on channel {ch}. Trying channel { {6:7,7:6}.get(ch) } instead!")
            ch = {6:7,7:6}.get(ch)
            rf_ts = self._get_ts(ch)
        assert rf_ts.size, f"Spill contains no RF data on channel {ch}"

        prescaler = {CH_RF_DIV20: 20, CH_RF_AUX:self.header['rfPrescaler'].item()}.get(ch)
        return rf_ts, prescaler
    
    def rf_period(self, time='all'):
        """RF period (inverse bunch frequency)
        
        :param time: determines at which points in time the RF period is returned, supported values are (all values in seconds):
                     - 'all'       return (time_array, rf_period_array) for all available data points
                     - 'mean'      return mean RF period
                     - np.ndarray  return array of RF periods for each given time (in s)
        """
        ts, prescaler = self._get_rf()
        t = self._time_till_start((ts[:-1]+ts[1:])/2)
        dt = np.diff(ts)/prescaler*self.header['tdcTickSecs'].item() # RF period in sec
        
        if time == 'all':
            return t, dt
        elif time == 'mean':
            return np.mean(dt)
        else:
            return np.interp(time, t, dt)
    
    def hittimes_rf(self, phase=True, offset=0):
        """Particle arrival times (in s or rad) with respect to RF clock
        
        :param phase: if true, convert arrival time in s to RF phase in rad
        :param offset: RF offset (in s or rad) to add
        """
        
        # timestamps of particle hits
        ts = self._get_ts(self.header['detectorChannel'].item())
        # timestamps of rf ticks (one tick every prescaler RF cycles)
        rf_ts, prescaler = self._get_rf()
        rf_dt = np.diff(rf_ts)/prescaler
        
        # for each ts, find the preceeding rf_ts
        idx = np.searchsorted(ts, rf_ts) # index of first ts per rf_ts
        di = np.diff(idx, append=len(ts)) # number of consecutive ts per rf_ts
        ts = ts[idx[0]:] # ignore timestamps before first rf tick
        previous_rf_ts = np.repeat(rf_ts, di) # make it match the shape of ts...
        current_rf_dt = np.repeat(np.append(rf_dt, rf_dt[-1]), di) # ...and assume rf does not change after last datapoint
        
        # subtract the preceeding rf_ts from each ts
        ts -= previous_rf_ts # now relative to every 20 rf ticks
        # apply custom offset
        if offset:
            if phase:
                ts = ts + offset/(2*np.pi) * current_rf_dt
            else:
                ts = ts + offset/self.header['tdcTickSecs'].item()
        # calculate the modulo
        ts = ts % current_rf_dt # now relative to every 1 rf tick
        
        if phase:
            return ts/current_rf_dt * 2*np.pi # to phase in rad
        else:
            return ts * self.header['tdcTickSecs'].item() # to time in seconds    
    
    def segment(self, t_start, t_stop):
        """Return a slice of this spill in given time interval"""
        ts_start, ts_stop = [self.ts[0] + (t - self.time_offset) / self.header['tdcTickSecs'].item() for t in (t_start, t_stop)]
        # ignore channel CH_VIRT_DRF which carries time relative to RF encoded as float instead of an absolute timestamp
        i_start = np.logical_and(self.ch != CH_VIRT_DRF, self.ts >= ts_start).argmax() # first index where condition is true
        i_stop = self.ts.size - 1 - np.logical_and(self.ch != CH_VIRT_DRF, self.ts < ts_stop)[::-1].argmax() # last index where condition is true
        return TDCSpill(self.header, self.data[i_start:i_stop+1], self._time_till_start(self.ts[i_start]))
    
    def segments(self, n, t_start=None, t_stop=None):
        """Iterator over segments in this spill
        
        :param n: Number of segments
        :param t_start: Time in seconds of first segment start
        :param t_stop: Time in seconds of last segment end
        """
        if t_start is None: t_start = self.time_offset
        if t_stop is None: t_stop = self.time_offset + self.length
        for i in range(n):
            trange = t_start + (t_stop-t_start)*i/n, t_start + (t_stop-t_start)*(i+1)/n
            yield trange, self.segment(*trange)        
    
    def __repr__(self):
        return f'TDCSpill({self.detector}, length={self.length:g}s, counts={self.counts})'

    def to_file(self, fname):
        """Save the spill to a file in compressed npz format
        """
        np.savez_compressed(fname, header=self.header, data=self.data, time_offset=self.time_offset)

    @classmethod
    def from_file(self, fname):
        """Load the spill from a file in compressed npz format
        """
        if not fname.endswith(".npz"): raise ValueError("fname must be a *.npz file. To load *.dat files, use TDCData.from_file(...) instead.")
        with np.load(fname) as f:
            return TDCSpill(f['header'], f['data'], f['time_offset'].item())
    
    
@dataclass
class TDCData:
    spills: list
    
    @classmethod
    def from_file(cls, fname, *, time_offset=0, max_load_spills=None):
        """Read in Micro Spill TDC data from a *.dat file saved with TDC application

        Args:
            fname (str): Path and filename to *.dat file
            time_offset (float): Adds a given time offset (in s) to the timestamps
            max_load_spills (int | None): Maximum number of spills to load from file, or None to load all.
                                          If not None, payload data will be loaded in chunks, and only the requested
                                          number of spills will be loaded into memory. Useful for large files.
        
        
        References: 
            Timo Milosic <T.Milosic@gsi.de>
                E-Mail exchange
            MicroSpillTDC-CLI-Tool
                https://git.acc.gsi.de/bi/MicroSpillTDC-CLI-Tool
        
        """
        raw = np.memmap(fname, dtype=np.uint8)
        
        # Read header
        header_version = raw[0]
        assert header_version == 2, f"Only files with header version 2 are supported, but got version {header_version}"
        header = raw[:216].view(dtype=[
            ('version',         'uint64'),    # Version of this format
            ('timestampUTC',    'int64'),     # 64-bit timestamp (UTC) via fesa::getSystemTime()
            ('tdcTickSecs',     'double'),    # TDC scale factor. Time corresponding to one TDC channel (via linear fit)
            ('rfPrescaler',     'uint64'),    # RF prescaler/divider
            ('detectorChannel', 'uint64'),    # Detector input [0-6]
            ('detectorName',    'S128'),      # Detector description used in the application
            ('rfChannel',       'uint64'),    # RF Channel. Will allow for external/aux in ch6 [default SIS18 master ch7]
            ('offsetRf',        'int64'),     # FESA: particleRefTime = std::fmod(curParticleTime - lastRfSampleTime + offsetRf, deltaRf)
            ('startEventID',    'uint64'),    # Event ID of start condition
            ('startEventMask',  'uint64'),    # Event Mask of start condition
            ('stopEventID',     'uint64'),    # Event ID of stop condition
            ('stopEventMask',   'uint64'),    # Event Mask of stop condition
        ])

        # Read payload
        payload = raw[216:].view(dtype='uint64')
        
        CHUNKSIZE = 1000000
        maxread = None if max_load_spills is None else CHUNKSIZE
        
        while True:
            # Parse payload
            channel = payload[:maxread] >> 60 # channel in upper 4 bits (half byte)
            timestamp = payload[:maxread] & 0xFFFFFFFFFFFF # timestamp in lower 48 bits (6 bytes)
            data = np.column_stack((channel, timestamp)).ravel().view(dtype=[
                ('channel', channel.dtype),       # Channel or event number
                ('timestamp', timestamp.dtype),   # Timestamp as multiple of tdcTickSecs value (see header)
            ])
        
            # Find spills in the dataset
            start = np.where(data['channel'] == CH_GATE_START)[0]
            end = np.where(data['channel'] == CH_GATE_END)[0]
            while len(end) > 0 and (len(start) == 0 or end[0] < start[0]):
                end = end[1:]  # ignore ends without start

            # Extract spills if we have read enough data
            if max_load_spills is None or len(end) >= max_load_spills or payload.size <= maxread:
                spills = [TDCSpill(header, data[s:e+1], time_offset=time_offset) for s, e in zip(start, end)]
                return TDCData(spills)
                
            else:
                # Need to load more data
                if len(end) > 0: CHUNKSIZE = (end[0] - start[0])
                maxread += CHUNKSIZE
    
    def __repr__(self):
        return 'TDCData(spills='+'\n               '.join([repr(s) for s in self.spills])+')'


