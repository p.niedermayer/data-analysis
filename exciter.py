

from data.plot_rfko_feedback_data import *
from mpl_toolkits.axes_grid1.inset_locator import inset_axes, zoomed_inset_axes, mark_inset
import tarfile
import glob
import os
import re
import yaml

cmap_petroff_bipolar = mpl.colors.LinearSegmentedColormap.from_list('Petroff bipolar', [petroff_colors[i] for i in reversed((2,6,1,3,9,0,4))])
cmap_petroff_bipolar.set_under(petroff_colors[8])
cmap_petroff_bipolar.set_over(petroff_colors[5])



def find_file(fname_wildcard, yml=False, *, exact_match=False, raise_if_not_found=True, archive=True):
    """Find the data or metadata file given a wildcard search string. Accounts for offsets by ±1 second in the filename"""
    
    wildcard = os.path.join("data", fname_wildcard)
    
    # search for file
    archive_found = None
    for fname in glob.glob(wildcard):
        if fname.endswith(".tar.xz"):
            archive_found = fname
            continue
            
        if yml == fname.endswith(".yml"):
            # file found
            if archive:
                archive_name = fname.strip('.yml')+".tar.xz"
                if not os.path.exists(archive_name):
                    # compress to archive
                    with tarfile.open(archive_name, "w:xz") as tar:
                        tar.add(fname, os.path.basename(fname))
                        other_fname = find_file(fname_wildcard, not yml, exact_match=False, raise_if_not_found=False, archive=False)
                        tar.add(other_fname, os.path.basename(other_fname))
                    print(f"Archive {archive_name} created")
            return fname
    
    # if not found, extract from archive
    if archive and archive_found:
        with tarfile.open(archive_found) as tar:
            tar.extractall(os.path.dirname(archive_found))
        print(f"Archive {archive_found} extracted")
        # then try again
        return find_file(fname_wildcard, yml, exact_match=exact_match, raise_if_not_found=raise_if_not_found, archive=False)
    
    # if still not found, search for alternative names
    if not exact_match:
        # maybe the timestamp is off by 1 second due to rounding, so check that
        path, file = os.path.split(wildcard)
        if match := re.match(r"(\d+)(.*)", file):
            prefix = os.path.join(path, f"{int(match.group(1)):0{len(match.group(1))}d}")
            for dt in (-1, +1):
                alt_prefix = os.path.join(path, f"{int(match.group(1))+dt:0{len(match.group(1))}d}")
                search = alt_prefix + match.group(2)
                for path in glob.glob(search):
                    # rename the file for clarity
                    newname = prefix + path[len(alt_prefix):]
                    os.rename(path, newname)
                    print("Fuzzy match: renamed", path, "-->", newname)

            # then try again
            return find_file(fname_wildcard, yml, exact_match=True, raise_if_not_found=raise_if_not_found, archive=archive)
                
    if raise_if_not_found:
        raise FileNotFoundError(f"No file found for query `{wildcard}` (yml={yml}, exact_match={exact_match})")
        


def plot_all(datasets, dt=None, mosaic=None, figsize=(14, 7), gridspec_kw=None, quality_std=False, colors=None, **kwargs):
    """
    kwargs: dt, t_start=None, t_stop=None, cumulative=False, fft=True, fmax=None, quality=True, quality_res=50
    """
    
    plot = SpillPlot(nrefs=len(datasets), figsize=figsize, dpi=120, mosaic=mosaic, gridspec_kw=gridspec_kw)
    plot.trace = {}
    plot.artist_quality_std = {}
    for i, (fname, label) in enumerate(datasets.items()):
        filename = find_file(fname)
        print("Loading", filename)
        data = Data.from_file(filename)
        trace = SpillPlotTraceData(data, dt, label=label, **kwargs)
        plot.update(trace, ref=i)
        if colors and fname in colors:
            for key in plot.artist:
                if i in plot.artist[key]:
                    plot.artist[key][i].set(color=colors[fname])
        if quality_std and plot.axis_quality is not None:
            plot.artist_quality_std[i] = plot.axis_quality.fill_between(
                *trace.trace_quality_uncertainty, alpha=0.2, lw=0, zorder=1.8,
                color=plot.artist_quality[i].get_color())
        plot.trace[i] = trace
    if plot.axis_quality_twin is not None and trace.quality_metric == "cv":
        plot.axis_quality_twin.axis("on" or "off")
    plot.autoscale()
    plot.draw()
    return plot

def plot_duty_over_time(datasets, dt, ncbins=1e3, t_start=None, t_stop=None, quality_metric="cv"):
    """
    """
    
    fig, ax = plt.subplots()
    for i, (fname, label) in enumerate(datasets.items()):
        fname = find_file(fname)
        print("Loading", fname)
        data = Data.from_file(fname)
        data = data.crop(t_start, t_stop)
        data = data.resample(dt)
        dt = data.dt
        
        # reshape into evaluation bins
        ncbins = int(ncbins)
        N = data.rate * data.dt
        N = N[:int(N.size/ncbins)*ncbins].reshape((-1, ncbins))
        T = np.linspace(data.t0, data.t0 + data.duration, N.shape[0])
        print(N.shape)
        # calculate metric
        if quality_metric == "cv":
            Cv = np.std(N, axis=1)/np.mean(N, axis=1)
            Cv_poisson = 1/np.mean(N, axis=1)**.5
            ax.plot(T, Cv, label=label)        
            ax.plot(T, Cv_poisson, ls=':', c='gray')
            ax.set(ylabel="Coefficient of variation $c_v=\\sigma/\\mu$", ylim=(0, 2))
        elif quality_metric == "maxmean":
            ratio = np.max(N, axis=1)/np.mean(N, axis=1)
            ax.plot(T, ratio, label=label)        
            ax.set(ylabel="Max-mean ratio", ylim=(1, 10))
        else:
            raise ValueError(f"Unknown quality_metric '{quality_metric}' not in ('cv', 'maxmean').") 
        
        
        print('dt_count', dt*1e6, 'us')
        print('dt_eval', dt*ncbins*1e3, 'ms')
    
    ax.set(title=f"Spill quality over time\n($\\Delta t_\\mathrm{{count}}$ = {dt*1e6:g} µs, $\\Delta t_\\mathrm{{evaluate}}$ = {dt*ncbins*1e3:g} ms)",
           xlabel='Time in spill $t$ / s')
    ax.legend()
    ax.grid(color='#ddd')
    
    if quality_metric == "cv":
        # Twin axes: duty factor
        at = ax.twinx()
        e = 1 # minor ticks every (spill duty factor percent value)
        cv2duty = lambda cv: 100/(1+cv**2)
        duty2cv = lambda du: (100/du-1)**.5
        at.set(ylabel="Spill duty factor   $F=\\langle N \\rangle^2/\\langle N^2 \\rangle$")
        at.yaxis.set_major_locator(TwinFunctionMajorLocator(ax.yaxis, cv2duty, duty2cv, e))
        at.yaxis.set_major_formatter(mpl.ticker.FuncFormatter(lambda cv, i: f'${cv2duty(cv):.0f}\,$%'))
        at.yaxis.set_minor_locator(mpl.ticker.FixedLocator(duty2cv(np.arange(e, 101, e))))
        # handle axis limits updates
        at.set(ylim=ax.get_ylim())
        ax.callbacks.connect("ylim_changed", lambda a: at.set(ylim=a.get_ylim()))
        at.set_navigate(False) # we maintain limits ourselves
        ax.set_zorder(at.get_zorder()+1)
    
    return fig, ax


def plot_quality_2d(fname, t_start=None, t_stop=None, quality_metric="cv",
                    resolution=200, cmap=cmap_petroff_bipolar or 'magma_r' or 'gist_ncar_r',
                    min_evaluations_per_spill=1,
                    min_counting_bins_per_evaluation=1,
                    std=False,
                    figsize=None,
                   ):
    fname = find_file(fname)
    print("Loading", fname)
    data = Data.from_file(fname)
    data = data.crop(t_start, t_stop)

    max_rebin = int(data.duration/data.dt/min_counting_bins_per_evaluation)
    rebins = np.unique(np.geomspace(1, max_rebin, resolution, dtype=int))
    dt_counts = data.dt*rebins        

    max_dt_eval = data.duration/min_evaluations_per_spill
    dt_evals = np.geomspace(data.dt*min_counting_bins_per_evaluation, max_dt_eval, resolution)

    quality = np.nan * np.empty((dt_counts.size, dt_evals.size, 3))
    timescales = np.meshgrid(dt_counts, dt_evals, indexing='ij')

    for i, dt_count in enumerate(dt_counts):            
        # rebin into counting bins
        rebin = int(dt_count/data.dt)
        dt_count = data.dt*rebin
        size = rebin*int(data.rate.size/rebin)
        NN = dt_count*np.reshape(data.rate[:size], (-1, rebin)).mean(axis=1)

        for j, dt_eval in enumerate(dt_evals):
            # reshape into evaluation bins
            ncbin = int(dt_eval/dt_count)
            if ncbin < min_counting_bins_per_evaluation: continue
            dt_eval = dt_count*ncbin
            N = NN[:int(NN.size/ncbin)*ncbin].reshape((-1, ncbin))

            # calculate metric
            with np.errstate(invalid='ignore', divide='ignore'):
                mean, std, max = np.mean(N, axis=1), np.std(N, axis=1), np.max(N, axis=1)
                if quality_metric == "cv":
                    val = Cv = std/mean
                    val_limit = Cv_poisson = 1/mean**.5
                    # Cv_poisson = 1/np.mean(N / 9428.6, axis=1)**.5 # scaling factor for IC for elog 844
                elif quality_metric == "maxmean":
                    val = max/mean
                    val_limit = np.NaN * mean
                else:
                    raise ValueError(f"Quality metric '{quality_metric}' not implemented. Valid choices are 'cv' or 'maxmean'.")

                val = val[np.isfinite(val)]
                val_limit = val_limit[np.isfinite(val_limit)]

            #plot.append((dt_count, np.nanmean(val), np.nanstd(val), np.mean(val_limit)))
            quality[i,j,:] = np.nanmean(val), np.nanstd(val), np.mean(val_limit)
            timescales[0][i,j] = dt_count
            timescales[1][i,j] = dt_eval


    fig, ax = plt.subplots(figsize=figsize, constrained_layout=True)
    if std:
        c = ax2.pcolormesh(*timescales, quality[:,:,1], shading='nearest', cmap=cmap, vmin=0, vmax=1, rasterized=True)
        fig2.colorbar(c, label="Std of " + {"cv": "Coefficient of variation $c_v=\\sigma/\\mu$",
                               "maxmean": "Max-mean ratio"}[quality_metric])
    else:
        c = ax.pcolormesh(*timescales, quality[:,:,0], shading='nearest', cmap=cmap, rasterized=True,
                          vmin=dict(cv=0, maxmean=1)[quality_metric], vmax=dict(cv=1.5, maxmean=7.5)[quality_metric])
        fig.colorbar(c, label={"cv": "Coefficient of variation $c_v=\\sigma/\\mu$",
                               "maxmean": "Max-mean ratio"}[quality_metric],
                     cax=inset_axes(ax, width="60%", height=0.2, loc="lower right", bbox_to_anchor=[0,0.15,0.95,1], bbox_transform=ax.transAxes),
                     extend='max', orientation="horizontal")
    
    ax.set(
        xlabel="Detector resolution   $\\Delta t_\\mathrm{{count}}$ / s", xscale='log',
        ylabel="Time scale   $\\Delta t_\\mathrm{{evaluate}}$ / s", yscale='log',
    )

    return fig, ax, timescales, quality


def twin_y_duty_factor(axis_cv, equation=False):
    return twin_ax_duty_factor(axis_cv, equation, "ver")

def twin_ax_duty_factor(axis_cv, equation=False, orientation="hor"):
    xy = "x" if orientation[0].lower() in "xh" else "y"
    ax = axis_cv
    ax.set(**{f"{xy}label":"Coefficient of variation $c_v"+("=\\sigma/\\mu" if equation else "")+"$"})
    
    # Twin axes: duty factor
    at = ax.twiny() if xy=="x" else ax.twinx()
    e = 1 # minor ticks every (spill duty factor percent value)
    cv2duty = lambda cv: 100/(1+cv**2)
    duty2cv = lambda du: (100/du-1)**.5
    at.set(**{f"{xy}label":"Spill duty factor $F"+("=\\langle N \\rangle^2/\\langle N^2 \\rangle" if equation else "")+"$"})
    axis = at.xaxis if xy=="x" else at.yaxis
    axis.set_major_locator(TwinFunctionMajorLocator(ax.xaxis if xy=="x" else ax.yaxis, cv2duty, duty2cv, e))
    axis.set_major_formatter(mpl.ticker.FuncFormatter(lambda cv, i: f'${cv2duty(cv):.0f}\,$%'))
    axis.set_minor_locator(mpl.ticker.FixedLocator(duty2cv(np.arange(e, 101, e))))
    # handle axis limits updates
    callback = (lambda a: at.set(xlim=a.get_xlim())) if xy=="x" else (lambda a: at.set(ylim=a.get_ylim()))
    ax.callbacks.connect(f"{xy}lim_changed", callback)
    at.set_navigate(False) # we maintain limits ourselves
    ax.set_zorder(at.get_zorder()+1)
    if xy=="x":
        ax.set(xlim=ax.get_xlim()) # force update once to initial values
    else:
        ax.set(ylim=ax.get_ylim()) # force update once to initial values
    


    
    
# Related to optimizer

def load_opti_log(logfile):
    history = []
    with open(logfile) as file:
        for i, line in enumerate(file):
            try:
                if line.startswith("[Optimizer] Variables"):
                    variables = line.split(":")[1].strip()
                    variables = [x.strip(" \"'") for x in variables.strip(" []").split(",") if x]
                if line.startswith("[Optimizer] Step"):
                    settings, objective = line.split(":")[1].split("-->")
                    settings = [float(x) for x in settings.strip(" []").split(" ") if x]
                    objective = float(objective)
                    history.append((settings, objective))
                if line.startswith("[Optimizer] Options"):
                    options = eval(line.split(":", maxsplit=1)[1].strip())
            except:
                print(f"Error parsing line {i+1} in file {logfile}:")
                print(line)
                raise
    val = np.array([x for x,o in history])
    obj = np.array([o for x,o in history])
    return options, variables, val, obj





# Functions to compare metadata for two or more saved data files

def compare(dicts, skip_missing=True):
    #print(dicts)
    differences = []
    keys = {k for dic in dicts for k in dic or {}}
    for key in keys:
        values = [(dic or {}).get(key) for dic in dicts]
        if skip_missing and None in values: continue
        if dict in [type(v) for v in values]:
            for diff in compare(values, skip_missing=skip_missing):
                differences.append([key + " : " + diff[0], *diff[1:]])
        else:
            u = None
            for v in values:
                if v is None: continue
                if u is None:
                    u = v
                elif v != u:
                    # difference detected
                    differences.append([key, *values])
                    break
    return differences
    #for key in dicts.keys()
    #for key, value in dicts

def _load_yml(fname):
    with open(fname, 'r') as yml:
        raw = yml.read()
        # fix an issue with tuple vs. list
        raw = raw.replace('(', '[').replace(')',']')
        return yaml.safe_load(raw)

def yml_diff(fnames, skip_missing=True, ignore="_|(opti)_.*"):
    meta = {}
    for fname in fnames:
        meta[fname] = _load_yml(find_file(fname, yml=True))
    
    # compare
    diff = compare(meta.values(), skip_missing=skip_missing)
    
    # filter for ignored patterns
    diff = [d for d in diff if not ignore or not re.match(ignore, d[0])]

    # print table
    diff.append(["", *fnames])
    diff = sorted(diff, key=lambda v: v[0])
    s = [max([len(str(v[i])) for v in diff]) for i in range(len(diff[0]))]
    for val in diff:
        for i, v in enumerate(val):
            print(f"{'' if v is None else str(v):>{s[i]}}", end="  ")
        print()
    print()
    
    #df = pandas.DataFrame(diff)#, columns=["", *meta.keys()])
    #return df

def yml_signals(fnames):
    meta = {}
    for fname in fnames:
        fullname = find_file(fname, yml=True)
        meta[fname] = _load_yml(fullname)
        meta[fname]["filename.yml"] = fullname
        
    for key, yml in meta.items():
        print(f"{key}   " + (fnames[key] if isinstance(fnames, dict) else '') + "   " + yml["filename.yml"])
        sig = yml.get('exciter_signals_0') or yml.get('sig')
        if sig is None: continue
        frev = float(sig.get('frev', 0))
        for i in range(1, 7):
            if sig[f'ex{i}_enabled']:
                out = f" Signal {i}  {str(sig[f'ex{i}_source']):30s}   a={sig.get(f'aex{i}'):.2f}   q={sig.get(f'qex{i}'):.4f}   f={frev*sig.get(f'qex{i}'):12.3f} Hz"
                if sig.get(f'dqex{i}', 0):
                    out += f"   dq={sig.get(f'dqex{i}', 0):.4f}   df={frev*sig.get(f'dqex{i}'):12.3f} Hz"
                if "dual_fm" in str(sig[f'ex{i}_source']) and sig.get("sweep_period", 0):
                    out += f"   Tsweep={sig.get('sweep_period')*1e3:g} ms"
                out += f"   frev = {frev:g} Hz"
                print(out)
        print()

def yml_feedback(fnames):
    meta = {}
    for fname in fnames:
        meta[fname] = _load_yml(find_file(fname, yml=True))
    
    for key, yml in meta.items():
        print(f"{key}   " + (fnames[key] if isinstance(fnames, dict) else ''))
        sig = yml.get('level_controller_0') or yml.get('ctrl')
        print(f" Parameters:  nparticles={sig['nparticles']} target_rate={sig['target_rate']} output_level={sig['output_level']}")
        if sig['feedforward']:
            print(f" Feedforward: y_pre={sig['_y_pre']}")
        else:
            print(f" Feedforward: OFF")
        if sig['feedback']:
            print(f" Feedback:    ta={sig['ta_set']} kp={sig['kp_npart_set']} ti={sig['ti_set']} td={sig['td_set']} ")
        else:
            print(f" Feedback:    OFF")
        print()



