# Data analysis utils

This repository is a collection of data analysis utils for reading and evaluating data.

## Usage

Add it as submodule to you data analysis repository
```
git submodule add git@git.gsi.de:p.niedermayer/data-analysis.git analysis_utils

# following external library is required only for reading data from Lassie application
cd analysis_utils
git submodule init
git -c http.sslVerify=false submodule update data/bdiolib
```

### Examples

**Tip**: In jupyter notbooks press SHIFT+TAB to show method signature and docstrings

Usage examples can be found in the [examples.ipynb](examples.ipynb) Jupyter notebook, but since this repo is evolving rapidly, examples might not be up to date. Please refer to dosctrings and autocompletion feature of your IDE.

More examples:
- https://git.gsi.de/p.niedermayer/data-analysis-2022-05-06-exciter/-/blob/main/analysis.ipynb
- https://git.gsi.de/p.niedermayer/data-analysis-2022-05-08-btf/-/blob/main/analysis.ipynb
- https://git.gsi.de/p.niedermayer/data-analysis-2022-06-28-ko-extraction/-/blob/master/analysis.ipynb





